# PS1 Game ID transmisson (MemCard PRO / PS1Digital)

The game id is sent over the PS1's controller bus using custom commands.
Both MemCard PRO and PS1Digital can read those game ids and can automatically configure themselves with this data:

Examples:

- MemCard PRO can automatically select the proper virtual memory card for a specific game
- PS1Digital can automatically load settings made for a specific game only.
- The game's name can be displayed on the MemCard PRO / PS1Digital's OSD using a game database used by both devices.
- MemCard PRO has some extra commands which allow the sender to control some of its functions, like channel and card switching


## Game ID:

PS1Digital and MemCard PRO expect the whole Game ID to be passed to them, including the mount prefix and full path. I.e.:
```
cdrom:\SLUS_000.03;1
```

## The PAD / Memory Card Bus
The commands are passed to the pad / memory card bus. These two are shared between each port, and the only thing that distinguishes between a pad command or a Memory Card command, is the "Bus Select" byte. There are only two, and this is the first byte sent. The bus is a full-duplex SPI with two ports selected by the `CS` signal. Each port then selects the device, by the first byte in the byte sequence sent:
- `0x42`: Pad Bus
- `0x81`: Memory Card Bus

The second byte that follows is the `device command`. This is the command which tells the device what the active operation is.
Different commands for the PS1D and the Memory Card are explained below.  
Following the `device command` the rest of the transfer is customisable. For PS1D and MCP, this usually is one reserved byte at the moment, followed by the length of the transmission that follows.

## **Commands**

### **MemCard PRO Ping (to check if the device is present)**
This command is useful if the application needs to check for the presence of a MemCard PRO in the slot being queried.

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x20`| `0x00` | Ping Command |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `0x00` | Reserved |
| 0x04 |`0x00`| `0x27` | Card Present |
| 0x05 |`0x00`| `0xFF` | Termination Signal |


### **GameID Send**
This command should be used to send the GameID to the pad bus.  
**Please Note** that while the MemCard PRO will actively respond to the bytes on the `DAT` and `ACK` lines, PS1D is only a bus sniffer, so it won't. For the PS1D, the application needs to ignore `DAT` and `ACK` being unresponsive.


| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 | `0x81` |`0xFF`| Memory Card Bus select  |
| 0x01 | `0x21` |`0x00`| GAMEID command start |
| 0x02 | `0x00` |`0x00`| Reserved |
| 0x03 | `strlen` |`0x00`| Length of the game id transmitted (`strlen`) |
| 0x04-0xFF | `data` |`Prev Byte`| Game id data as string |

### **MemCard PRO Previous Channel**
MemCard PRO groups together 8 "channels" per virtual card. Each channel is in reality a 15-block virtual memory card.  
This command instructs the MemCard PRO to mount the previous channel of the Virtual Memory Card.

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x22`| `0x00` | Previous Channel |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `0x00` | Reserved |
| 0x04 |`0x00`| `0x20` | Success |
| 0x05 |`0x00`| `0xFF` | Termination Signal |

### **MemCard PRO Next Channel**
This command instructs the MemCard PRO to mount the next channel of the Virtual Memory Card.

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x23`| `0x00` | Next Channel |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `0x00` | Reserved |
| 0x04 |`0x00`| `0x20` | Success |
| 0x05 |`0x00`| `0xFF` | Termination Signal |


### **MemCard PRO Previous Card**
This command instructs the MemCard PRO to mount the previous folder (card)

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x24`| `0x00` | Previous Card |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `0x20` | Success |
| 0x04 |`0x00`| `0xFF` | Termination Signal |

### **MemCard PRO Next Card**
This command instructs the MemCard PRO to mount the next folder (card)

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x25`| `0x00` | Next Card |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `0x00` | Reserved |
| 0x04 |`0x00`| `0x20` | Success |
| 0x05 |`0x00`| `0xFF` | Termination Signal |

### **MemCard PRO Get Card Name (Not Yet Implemented)**
This command instructs the MemCard PRO to pass the name of the currently mounted card.

| Byte | CMND | DATA | Comment |
| ---- | ---- | ----------- | ------- |
| 0x00 |`0x81`| `0xFF` | Memory Card Bus Select |
| 0x01 |`0x26`| `0x00` | Get Card Name |
| 0x02 |`0x00`| `0x00` | Reserved |
| 0x03 |`0x00`| `strlen` | Name length (`strlen`) |
| 0x04-0xFF |`0x00`| `data` | Card Name Data as String |
